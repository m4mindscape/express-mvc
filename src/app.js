import Application                    from "./core/application/Application";
import DI                             from "./core/container/DI";
import ServiceRegistrationControllers from "./services/ServiceRegistrationControllers";
import ServiceRouterHelper            from "./services/ServiceRouterHelper";
import twig                           from 'twig';
import Server from "./modules/device/server/Server";
import MandrillService from "./services/MandrillService"
import RtbRegexToSite from "./services/RtbRegexToSite"

twig.cache(false);

let container = new DI();

container.set('DIR', __dirname);
container.set('PORT', 3500);
container.set('CONFIG', {
    views: {
        path: `${__dirname}\\views`,
        engine: 'twig',
        cache: 'false'
    },
    public: `${__dirname}\\public`
});

container.setObject(Application, null);
container.setObject(ServiceRouterHelper, null);
container.setObject(ServiceRegistrationControllers, null);
container.setObject(MandrillService, null);
// container.setObject(RtbRegexToSite, null);