import MandrillTemplateModel from "../models/MandrillTemplateModel"

export default class MandrillTemplatesRepository {
	constructor() {
		/**
		 * @type {Map<string, MandrillTemplateModel>} this.repository
		 * */
		this.repository = new Map();
	}

	/**
	 * @param {MandrillTemplateModel} template
	 * */
	add(template) {
		this.repository.set(template.slug, template);
	}

	/**
	 * @returns {MandrillTemplateModel[]}
	 * */
	getAll() {
		return [...this.repository.values()];
	}

	/**
	 * @returns {MandrillTemplateModel}
	 * */
	getByName(name) {
		if (this.repository.has(name)) {
			return this.repository.get(name);
		}
		return null;
	}

	/**
	 * @returns {MandrillTemplateModel[]}
	 * */
	getAllRtbTemplates() {
		return [...this.repository.values()].filter(template => template.slug.indexOf('miro-') === -1);
	}

	/**
	 * @returns {MandrillTemplateModel[]}
	 * */
	getAllMiroTemplates() {
		return [...this.repository.values()].filter(template => template.slug.indexOf('miro-') > -1);
	}

	/**
	 * @returns {MandrillTemplateModel|null}
	 * */
	getPrevTemplate(key) {
		let templates = this.getAll();
		let template = null;
		for (let count in templates) {
			if (templates[count].slug === key) {
				template = templates[parseInt(count) - 1];
				break;
			}
		}

		return template;
	}

	/**
	 * @returns {MandrillTemplateModel|null}
	 * */
	getNextTemplate(key) {
		let templates = this.getAll();
		let template = null;
		for (let count in templates) {
			if (templates[count].slug === key) {
				template = templates[parseInt(count) + 1];
				break;
			}
		}

		return template;
	}

	/**
	 * @returns {MandrillTemplateModel[]};
	 * */
	getRemoveTemplates() {
		return [...this.repository.values()].filter(template => template.remove === true);
	}
}