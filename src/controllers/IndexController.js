import Controller from "../core/controller/Controller";
import {IControllerAddRout, IControllerInit} from "../core/controller/InterfaceController";
import {RouterGet} from "../core/application/Router";
import {IContainer} from "../core/container/InterfaceDi"
import Server from "../modules/device/server/Server"
import MandrillTemplatesRepository from "../repository/MandrillTemplatesRepository"
import MandrillService from "../services/MandrillService"

export default class IndexController extends Controller {
	[IControllerInit]() {
		this[IControllerAddRout](new RouterGet('/', this.index));
		// this[IControllerAddRout](new RouterGet('/upload-all-templates', this.upload));
	}

	index() {
		let repository = this.controller[IContainer].get(MandrillTemplatesRepository);

		this.res.render('pages/index', {
			title: 'Dashboard',
			message: 'test',
			newTemplates: repository.getAll()
		})
	}

	upload() {
		let repository = this.controller[IContainer].get(MandrillTemplatesRepository);
		let mandrillService = this.controller[IContainer].get(MandrillService);

		this.res.render('pages/index', {
			title: 'Upload',
			message: 'Upload all templates'
		})
		mandrillService.uploadTemplates(repository.getAll());
	}

}