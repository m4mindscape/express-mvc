import Controller from "../core/controller/Controller"
import {IControllerAddRout, IControllerInit} from "../core/controller/InterfaceController"
import {RouterGet} from "../core/application/Router"
import {IContainer} from "../core/container/InterfaceDi"
import MandrillTemplatesRepository from "../repository/MandrillTemplatesRepository"
import MandrillService from "../services/MandrillService"

export default class RemoveTemplatesMandrillController extends Controller {
	[IControllerInit]() {
		this[IControllerAddRout](new RouterGet('/remove/all/', this.removePage))
		this[IControllerAddRout](new RouterGet('/list/templates/remove/all', this.getListRemove))
		this[IControllerAddRout](new RouterGet('/remove/template/:id', this.remove))
		/**
		 * @type {MandrillTemplatesRepository} mandrillTemplatesRepository
		 * */
		this.mandrillTemplatesRepository = this[IContainer].get(MandrillTemplatesRepository);
		/**
		 * @type {MandrillService} mandrillTemplatesRepository
		 * */
		this.mandrillService = this[IContainer].get(MandrillService);
	}

	removePage() {
		this.res.render('pages/remove/all/index.twig', {
			title: 'test'
		})
	}

	getListRemove() {
		let list = this.controller.mandrillTemplatesRepository.getRemoveTemplates();
		let out = [];

		for(let template of list) {
			out.push(template.slug);
		}
		this.res.json({
			count: out.length,
			list: out
		})
	}

	remove() {
		let _this = this;
		let template = _this.controller.mandrillTemplatesRepository.getByName(_this.getParam('id'));

		this.controller.mandrillService.remove(template.slug, (result) => {
			if (result.slug !== undefined) {
				console.dir(result.slug + ' - REMOVE');
			}
			else {
				console.dir(result);
			}
			_this.res.json({
				status: true
			})
		});
	}
}