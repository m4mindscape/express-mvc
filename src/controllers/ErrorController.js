import {IControllerInit, IControllerRouterHelper} from "../core/controller/InterfaceController"
import error from 'http-errors';
import Controller from "../core/controller/Controller"
import {RouterGet} from "../core/application/Router"

export default class ErrorController extends Controller {
	[IControllerInit]() {
		this[IControllerRouterHelper].set(new RouterGet('*', this.pageNotFound))
	}

	pageNotFound() {
		this.res.status(404);
		this.res.render('error');
	}
}