import {RouterGet} from "../core/application/Router";
import Controller from "../core/controller/Controller";
import {IControllerAddRout, IControllerInit, IControllerRouterHelper} from "../core/controller/InterfaceController";
import {IContainer} from "../core/container/InterfaceDi"
import MandrillTemplatesRepository from "../repository/MandrillTemplatesRepository"
import {parse} from 'node-html-parser'
import cheerio from 'cheerio';
import Redesign from "../helpers/Redesign"

export default class TemplateController extends Controller {
	[IControllerInit]() {
		this[IControllerAddRout](new RouterGet('/template/:id', this.template));
		this[IControllerAddRout](new RouterGet('/template/view/:id', this.view));
		this[IControllerAddRout](new RouterGet('/template/preview/:id', this.preview))
		this[IControllerAddRout](new RouterGet('/template/migration-rtb/:id', this.merge))
		this[IControllerAddRout](new RouterGet('/'))
	}

	index() {
		this.res.render('pages/template', {
			title: 'Template'
		});
	}

	merge() {
		/**
		 * @type {MandrillTemplatesRepository}
		 * */
		let repository = this.controller[IContainer].get(MandrillTemplatesRepository);

		let miroTemplate = repository.getByName(this.getParam('id'));
		let rtbTemplate = repository.getByName(miroTemplate.slug.replace('miro-', ''));

		miroTemplate.code = rtbTemplate.code;
		miroTemplate.subject = rtbTemplate.subject;
		miroTemplate.fromEmail = rtbTemplate.fromEmail;
		miroTemplate.labels = rtbTemplate.labels;

		this.res.redirect(`/template/view/${miroTemplate.slug}`);
	}

	view() {
		/**
		 * @type {MandrillTemplatesRepository}
		 * */
		let repository = this.controller[IContainer].get(MandrillTemplatesRepository);
		let template = repository.getByName(this.getParam('id'));

		let next = repository.getNextTemplate(template.slug);
		let prev = repository.getPrevTemplate(template.slug);

		this.res.render('pages/template/view', {
			title: this.getParam('id'),
			template: template,
			next: next,
			prev: prev
		})
	}

	preview() {
		/**
		 * @type {MandrillTemplatesRepository}
		 * */
		let repository = this.controller[IContainer].get(MandrillTemplatesRepository);
		let template = repository.getByName(this.getParam('id'));

		if (template) {
			this.res.send(repository.getByName(this.getParam('id')).code ? repository.getByName(this.getParam('id')).code : 'Темплейт пустой');
		}
		else  {
			this.res.send(`Темплейт не найден! ${this.getParam('id')}`)
		}
	}

	printChild(Element) {
		let map = {};
		map['replace'] = false;

		// Логотип
		if (Element.tagName === 'img') {
			if (Element.rawAttrs.indexOf('src="https://letter.realtimeboard.com/2015-09-17/logo-small.png" ') > -1) {
				map['replace'] = true;
			}
		}

		// Плашка
		if (Element.tagName === 'div') {
			if (Element.rawAttrs.indexOf('width: 80px; line-height: 30px; background: #FBCF47;') > -1) {
				map['replace'] = true;
			}
		}

		if (Element.tagName) {
			map['key'] = Element.tagName;
			map['value'] = [];
			map['classes'] = Element.classNames.join('.');
			map['id'] = Element.id;
			map['raw'] = Element.toString();

			if (Element.childNodes) {
				for (let element of Element.childNodes) {
					if (element.tagName !== undefined) {
						map['value'].push(this.printChild(element));
					}
				}
			}

			return map;
		}
		return null;
	}
}