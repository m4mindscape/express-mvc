import Controller from "../core/controller/Controller"
import {IControllerAddRout, IControllerInit} from "../core/controller/InterfaceController"
import ServiceRouterHelper from "../services/ServiceRouterHelper"
import {RouterGet} from "../core/application/Router"
import {IContainer} from "../core/container/InterfaceDi"
import MandrillTemplatesRepository from "../repository/MandrillTemplatesRepository"
import MandrillService from "../services/MandrillService"

export default class UpdateMiroTemplates extends Controller {
	[IControllerInit]() {
		this[IControllerAddRout](new RouterGet('/update/miro/', this.index));
		this[IControllerAddRout](new RouterGet('/update/miro/get-list-slug', this.getSlugList))
		this[IControllerAddRout](new RouterGet('/update/miro/:template', this.upload));

		this.repository = this[IContainer].get(MandrillTemplatesRepository);
		this.mandrill = this[IContainer].get(MandrillService);
	}

	index() {
		this.res.render('pages/update/miro', {
			title: 'Update all templates',
			listTemplate:  this.controller.repository.getAllMiroTemplates()
		});
	}

	getSlugList() {
		let templates = this.controller.repository.getAllMiroTemplates();
		let out = [];

		for (let template of templates) {
			out.push(template.slug);
		}

		this.res.json({
			list: out,
			count: out.length
		})
	}


	upload() {
		let _this = this;
		let template = _this.controller.repository.getByName(_this.getParam('template'));

		/**
		 * @type {MandrillService} mandrill
		 * */
		let mandrill = _this.controller.mandrill;


		mandrill.updateTemplate(template, (data) => {
			_this.res.json({
				status: true,
				slug: _this.getParam('template'),
				result: data
			})
		});
	}
}