import Controller from "../core/controller/Controller"
import {IControllerAddRout, IControllerInit} from "../core/controller/InterfaceController"
import {RouterGet} from "../core/application/Router"
import {IContainer} from "../core/container/InterfaceDi"
import MandrillTemplatesRepository from "../repository/MandrillTemplatesRepository"
import MandrillService from "../services/MandrillService"

import fs from 'fs';

export default class ControlController extends Controller {
	[IControllerInit]() {
		this[IControllerAddRout](new RouterGet('/control/merge/rtb-in-miro', this.mergeRtbInMiro));
		this[IControllerAddRout](new RouterGet('/control/update/all-miro', this.updateAllMiroTemplates));
		this[IControllerAddRout](new RouterGet('/control/create-file-in-repository/', this.createRepositoryFile))

		/**
		 * @type {MandrillTemplatesRepository} mandrillTemplatesRepository
		 * */
		this.mandrillTemplatesRepository = this[IContainer].get(MandrillTemplatesRepository);
		this.mandrillService = this[IContainer].get(MandrillService);
	}

	mergeRtbInMiro() {
		for (let template of this.controller.mandrillTemplatesRepository.getAllMiroTemplates()) {
			let rtb = this.controller.mandrillTemplatesRepository.getByName(template.slug.replace('miro-', ''));

			if (rtb) {
				template.code = rtb.code;
				template.subject = rtb.subject;
				template.fromEmail = rtb.fromEmail;
				template.fromName = rtb.fromName
			}
		}

		this.res.redirect('/');
	}

	updateAllMiroTemplates() {
		let templates = this.controller.mandrillTemplatesRepository.getAllMiroTemplates();
		if (templates.length > 0) {
			let count = 0;

			let interval = setInterval(() => {

				if (count >= templates.length - 1) {
					console.dir('Update success');
					clearInterval(interval);
				}
				let temp = templates[count];

				temp.code += '<h4>Test</h4>';
				this.controller.mandrillService.updateTemplate(temp);
				count++;
			}, 500);
		}

		this.res.redirect('/');
	}

	createRepositoryFile() {

		let templates = this.controller.mandrillTemplatesRepository.getAllMiroTemplates();

		let path = 'C:\\projects\\email\\templates\\';


		for (let template of templates) {
			fs.writeFile(path + template.slug + '\\index.html', template.code, (stat) => {
				console.dir(stat);
			});
		}
		this.res.redirect('/');
	}
}