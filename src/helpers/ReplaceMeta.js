import MandrillTemplateModel from "../models/MandrillTemplateModel"

export default class ReplaceMeta {

	/**
	 * @param {MandrillTemplateModel} template
	 * */
	static replaceMeta(template) {
		if (typeof template.fromEmail === 'string') {
			template.fromEmail = template.fromEmail.replace('@realtimeboard.com', '@miro.com');
		}

		template.code = template.code.replace(new RegExp('https://realtimeboard.com', 'g'), 'https://miro.com');
		template.code = template.code.replace(new RegExp('https://facebook.com/getmiro/', 'g'), 'https://www.facebook.com/TryMiro/');
		template.code = template.code.replace(new RegExp('https://help.realtimeboard.com', 'g'), 'https://help.miro.com');
	}
}
