import cheerio from "cheerio"
import MandrillTemplateModel from "../models/MandrillTemplateModel"

let newTemplate = [
	'daily-updates',
	'mentions',
	'invite-to-team',
	'invite-to-team-and-board',
	'invite-to-board',
	'add-to-board',
	'add-to-team',
	'add-to-team-and-board',
	'add-to-project'
];

export default class Redesign {

	constructor() {
		this.first = false
	}

	/**
	 * @param {MandrillTemplateModel} template
	 * */
	static redisign(template) {
		if (!template || !template.code || template.slug.indexOf('miro-') > -1) {
			return;
		}

		if (template.subject) {
			template.subject = template.subject.replace(new RegExp("(?!(http(s?):\\/\\/))(R|r)ealtime(B|b)oard(?!((\\.)(com|ru)))", 'g'), "Miro");
		}

		if (template.fromEmail) {
			template.fromEmail = template.fromEmail.replace(new RegExp("@realtimeboard.com", "g"), "@miro.com");
		}

		if (template.fromName) {
			template.fromName = template.fromName.replace(new RegExp("(?!(http(s?):\\/\\/))(R|r)ealtime(B|b)oard(?!((\\.)(com|ru)))", 'g'), "Miro")
		}

		template.code = template.code.replace(new RegExp("(R|r)ealtime(B|b)log", 'g'), "MiroBlog");
		template.code = template.code.replace(new RegExp("(?!(http(s?):\\/\\/))(R|r)ealtime(B|b)oard(?!((\\.)(com|ru)))", 'g'), "Miro");
		template.code = template.code.replace(new RegExp('https://letter.realtimeboard.com/slack-integration/slack-integration-2.gif', 'g'),
			'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/gifs/miro-slack.gif');
		template.code = template.code.replace(new RegExp('https://letter.realtimeboard.com/slack-integration/slack-integration-1.png', 'g'),
			'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/img/slack-integration-1.png');
		template.code = template.code.replace(new RegExp('https://letter.realtimeboard.com/slack-integration/slack-integration-3.png', 'g'),
			'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/img/slack-integration-3.png');
		template.code = template.code.replace(new RegExp('(#6699(F|f)(F|f))|(#(F|f)6(A|a)633)', 'g'), "#2A79FF");
		template.code = template.code.replace(new RegExp('(#(E|e)(E|e)(F|f)5(F|f)(F|f))', 'g'), '#f3f4f8');
		template.code = template.code.replace(new RegExp('https://www.facebook.com/Miro', 'g'), 'https://www.facebook.com/getmiro');
		template.code = template.code.replace(new RegExp('https://twitter.com/Miro', 'g'), 'https://twitter.com/mirohq');

		let fullLogo = true;

		for (let key of newTemplate) {
			if (template.slug.indexOf(key) >= 0) {
				console.dir(template.slug);
				fullLogo = false;
				break;
			}
		}

		let $ = cheerio.load(template.code, {decodeEntities: false});

		if (!template.custom) {
			$('div').each((i, el) => {
				if ($(el).css('background') === '#FBCF47' || $(el).css('background') === '#fbcf47') {
					$(el).css('background', '')
					template.custom = true;
				}
			})

			$('img[src="https://letter.realtimeboard.com/2015-09-17/logo-small.png"], img[src="https://letter.realtimeboard.com/2015-09-17/logo.png"], img[src="https://letter.realtimeboard.com/template-v2/sticker.png"]').attr(
				'src',
				'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/icons/miro-160x100.png').css({width: '80px', height: '50px'})
			$('img[src="https://letter.realtimeboard.com/2015-09-17/logo-small.png"]').parents('table').each((i, el) => {
				if (i == 1) {
					$(el).addClass('header');
					template.custom = true;
				}
			});

			$('img[src="https://letter.realtimeboard.com/2015-09-17/f.png"], img[src="https://letter.realtimeboard.com/templates/i-fb.png"]').parents('table').each((i, el) => {
				if (i === 1) {
					$(el).addClass('footer');

					$('.footer > tbody > tr:first-child > td').prepend(
						'<div id="CUSTOM_FOOTER" style="background-color: #FFF; padding: 42px 40px; margin-top: 20px;"><div style="margin-bottom: 4px; font-family: Helvetica; font-size: 14px; line-height: 20px; color: #050038; font-weight: bold;">RealtimeBoard is now Miro!</div><div style="font-family: Helvetica; font-size: 14px; line-height: 20px; color: #676486;">RealtimeBoard is now Miro. We have a new brand identity and a new name, Miro, that’s inspired by the amazing things our customers have built in our product. <a href="https://realtimeboard.com/new-brand/">Click here to read more</a>, or reach out to us with any feedback or questions.</div></div>');

					template.custom = true;
				}
			})

			if ($('.layout-3__logo, .daily-mention__logo').length > 0) {
				if (fullLogo) {
					$('.layout-3__logo > img, .daily-mention__logo > img').attr('src',
						'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/icons/miro-256x70.png');
				}
				else {
					$('.layout-3__logo > img, .daily-mention__logo > img').parent().css({width: '176px'})
					$('.layout-3__logo > img, .daily-mention__logo > img').attr('src',
						'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/logo.png').css({height: '30px'});
				}
			}

			if ($('.onboarding-1__logo').length > 0) {
				$('.onboarding-1__logo').css({'width': '80px', 'height': '50px'})
				$('.onboarding-1__logo > img').attr('src', 'https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/icons/miro-160x100.png')
			}

			if ($('.layout-3__footer, .daily-mention__footer, .onboarding-1__footer').length > 0) {
				if (fullLogo) {
					$('.layout-3__footer > tbody > tr > td, .daily-mention__footer > tbody > tr > td, .onboarding-1__footer > tbody > tr > td').prepend(
						'<div id="CUSTOM_FOOTER" style="background-color: #FFF; padding: 42px 40px; margin-top: 20px;"><div style="margin-bottom: 4px; font-family: Helvetica; font-size: 14px; line-height: 20px; color: #050038; font-weight: bold;">RealtimeBoard is now Miro!</div><div style="font-family: Helvetica; font-size: 14px; line-height: 20px; color: #676486;">RealtimeBoard is now Miro. We have a new brand identity and a new name, Miro, that’s inspired by the amazing things our customers have built in our product. <a href="https://realtimeboard.com/new-brand/">Click here to read more</a>, or reach out to us with any feedback or questions.</div></div>');
				}
				else {
					$('.layout-3__footer > tbody > tr > td, .daily-mention__footer > tbody > tr > td, .onboarding-1__footer > tbody > tr > td').prepend(
						'<div id="CUSTOM_FOOTER" style="background-color: #FFF; padding: 42px 40px; margin-top: 20px;"><div style="margin-bottom: 4px; font-family: Helvetica; font-size: 14px; line-height: 20px; color: #050038; font-weight: bold;">RealtimeBoard is now Miro!</div><div style="font-family: Helvetica; font-size: 14px; line-height: 20px; color: #676486;">We have a new brand identity and a new name, Miro, that’s inspired by the amazing things our customers have built in our product. <a href="https://realtimeboard.com/new-brand/">Click here to read more</a>, or reach out to us with any feedback or questions.</div></div>');
				}

				template.custom = true;
			}

			let element = $('a[href="https://vimeo.com/Miro"], a[href="https://www.youtube.com/user/MiroEng/videos/"]');
			while (1) {
				element = element.parent();

				if (element['0'] !== undefined) {
					if (element['0'].name == 'tr') {
						break;
					}
				}
				else  {
					element = null;
					break;
				}
			}

			if (element) {

				element.html(
					'<table align="center" class="miro__footer-ico-group" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top">'
					+ '                    <tr style="padding:0;text-align:left;vertical-align:top">'
					+ '                        <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;height:56px;hyphens:auto;line-height:1.43;margin:0;padding:0;text-align:left;vertical-align:middle;width:56px;word-wrap:break-word">'
					+ '                            <a href="https://facebook.com/getmiro/" target="_blank"'
					+ '                               style="Margin:0;color:#2a79ff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.43;margin:0;padding:0;text-align:left;text-decoration:none"><img'
					+ '                                    src="https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/facebook.png"'
					+ '                                    style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;height:auto;max-height:100%;max-width:100%;outline:0;text-decoration:none;width:auto"></a>'
					+ '                        </td>'
					+ '                        <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;height:56px;hyphens:auto;line-height:1.43;margin:0;padding:0;text-align:left;vertical-align:middle;width:56px;word-wrap:break-word">'
					+ '                            <a href="https://twitter.com/mirohq/" target="_blank"'
					+ '                               style="Margin:0;color:#2a79ff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.43;margin:0;padding:0;text-align:left;text-decoration:none"><img'
					+ '                                    src="https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/twitter.png"'
					+ '                                    style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;height:auto;max-height:100%;max-width:100%;outline:0;text-decoration:none;width:auto"></a>'
					+ '                        </td>'
					+ '                        <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;height:56px;hyphens:auto;line-height:1.43;margin:0;padding:0;text-align:left;vertical-align:middle;width:56px;word-wrap:break-word">'
					+ '                            <a href="https://www.linkedin.com/company/realtimeboard/" target="_blank"'
					+ '                               style="Margin:0;color:#2a79ff;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.43;margin:0;padding:0;text-align:left;text-decoration:none"><img'
					+ '                                    src="https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/in.png"'
					+ '                                    style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:block;height:auto;max-height:100%;max-width:100%;outline:0;text-decoration:none;width:auto"></a>'
					+ '                        </td>'
					+ '                    </tr>'
					+ '                </table>');
			}
			template.code = unescape($.html());
		}
	}
}