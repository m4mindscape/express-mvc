import cheerio from "cheerio"

export default class RedesignNew {
	/**
	 * @param {MandrillTemplateModel} template
	 * */
	static redisign(template) {
		if (!template || !template.code) {
			return;
		}


		let $ = cheerio.load(template.code, {decodeEntities: false});

		$('a[href="https://www.linkedin.com/company/realtimeboard/"]').attr('href', 'https://www.linkedin.com/company/mirohq/');

		let logo = $('img[src="https://rtb-production-eu-mail.s3.eu-west-1.amazonaws.com/miro/images/icons/miro-160x100.png"]');

		let parent = logo.parent().parent();
		if (parent.attr('align') === 'right') {
			parent.attr('align', 'left')
			logo.css('margin-left', '100px')

			parent = logo.closest('table').parent().parent().parent().parent();

			/*console.dir(parent['0'].name);*/

			parent = $(parent['0']).parent();

			parent.css({border: "3px solid #0099FF"})
		}

		template.code = unescape($.html());
	}
}