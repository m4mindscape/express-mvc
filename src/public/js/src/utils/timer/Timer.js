class Timer {
    constructor(time, callback) {

        this.time = time;
        this.remaining = time;
        this.callback = callback;
        this.timer = null;
        this.status = true;
        this.resume();
    }

    pause() {
        if (this.status) {
            clearTimeout(this.timer);
            this.remaining -= new Date() - this.start;
        }
    }

    resume() {
        if (this.status) {
            this.start = new Date();
            this.timer = setTimeout(this.callback, this.remaining);
        }
    }

    stop() {
        this.status = false;
        clearTimeout(this.timer);
    }

    getPercent() {
        return ((this.time - this.remaining) / this.time) * 100;
    }
}

export default Timer;