import DI from "./container/DI"
import {
	BubbleType,
	NotificationBubbleModule,
	NotificationBubbleAlert, NotificationBubbleInput, NotificationBubbleDialog
} from "./modules/notification/bubble/NotificationBubbleModule"
import {Websocket} from "./modules/websocket/Websocket"
import {HrefLock} from "./ui/HrefLock"


let container = new DI();

container.setObject(NotificationBubbleModule, {
	element: document.getElementById('bubble-alert'),
	close: {
		tag: 'i',
		class: ['fa', 'fa-fw', 'fa-times']
	}
});

for(let element of document.getElementsByClassName('js-chunk')) {
	element.addEventListener('mouseenter', function (e) {
		this.getElementsByClassName('js-code')[0].style.display = 'block';
	})
	element.addEventListener('mouseout', function (e) {
		this.getElementsByClassName('js-code')[0].style.display = 'none';
	})
}

container.setObject(Websocket);