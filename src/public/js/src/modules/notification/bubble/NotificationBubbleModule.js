import {
	IContainer,
	IContainerInitInjection,
	IContainerInjectionArgs,
	IContainerSetArgs
} from "../../../../../../core/container/InterfaceDi"
import Timer from "../../../utils/timer/Timer";

let GlobalArgs = null;

class NotificationBubbleModule {
	constructor() {
		this.elements = [];
		this[IContainer] = null;
		this[IContainerInjectionArgs] = null;
		this.block = null;
	}

	[IContainerSetArgs](args) {
		this[IContainerInjectionArgs] = args;
		GlobalArgs = args;

	}

	[IContainerInitInjection]() {
		this.block = this[IContainerInjectionArgs].element;
	}

	alert(element) {
		this.elements.push(element);
		this.block.appendChild(element.element);
		element.active = true;
	}
}

class NotificationBubble {
	constructor(title, message, type) {
		this.title = title ? title : 'Alert title';
		this.message = message ? message : 'Alert message';
		this.type = type ? type : BubbleType.default;
		this.timer = null;
		this._element = null;
		this._closeBtn = null;
		this._active = null;
		this.element = this.createBaseAlert();
	}

	get element() {
		return this._element;
	}

	set element(value) {
		this._element = value;
	}

	get closeBtn() {
		return this._closeBtn;
	}

	set closeBtn(value) {
		this._closeBtn = value;
	}

	addCloseEvent(callback) {
		this.closeElement.addEventListener('click', (e) => {
			this.active = false;

			if (typeof callback === 'function') {
				callback({
					status: 'close',
					alert: this
				});
			}
		});
	}

	addEventTimer(time) {
		this.time = time ? time : 10000;
		this.timer = new Timer(this.time, () => {
			this.active = false;
		});

		this._element.addEventListener('mouseover', () => {
			this.timer.pause();
		});

		this._element.addEventListener('mouseout', () => {
			this.timer.resume();
		});
	}

	set active(value) {
		this._active = value;

		if (value) {
			requestAnimationFrame(() => {
				this.element.classList.add("--show");
			});
		}
		else {
			requestAnimationFrame(() => {
				this.element.classList.remove("--show");
				this.element.addEventListener('transitionend', () => {
					this.element.remove();
				})
			});
		}
	}

	createBaseAlert() {
		let alert = document.createElement('div');
		let title = document.createElement('div');
		let message = document.createElement('div');

		alert.classList.add(this.type);
		alert.classList.add('bubble-alert-notify');

		if (this.title) {
			title.classList.add('bubble-alert-notify-title');
			title.appendChild(document.createTextNode(this.title));
			alert.appendChild(title);
		}

		if (this.message) {
			message.classList.add('bubble-alert-notify-message');
			message.appendChild(document.createTextNode(this.message));
			alert.appendChild(message);
		}

		return alert;
	}

	createCloseBtn() {
		let close = document.createElement('div');
		let icoClose = document.createElement(GlobalArgs.close.tag);
		icoClose.classList.add(...GlobalArgs.close.class);
		close.appendChild(icoClose);
		close.classList.add('bubble-alert-notify-close');
		this.element.appendChild(close);
		this.closeElement = close;
	}

	createFooter() {
		this.footer = document.createElement('div');
		this.footer.classList.add('bubble-alert-notify-footer');
		this.element.appendChild(this.footer);
	}
}

class NotificationBubbleAlert extends NotificationBubble {
	constructor(title, message, type, time) {
		super(title, message, type);
		this.createCloseBtn();
		this.addCloseEvent();
		this.addCloseEvent(() => {
			this.timer.stop();
		});
		this.addEventTimer(time);
	}
}

class NotificationBubbleInput extends NotificationBubble {
	constructor(title, message, type, args) {
		super(title, message, type);
		this.input = null;
		this.args = args;
		this.createFooter();
		this.createInput();
		this.createCloseBtn();
		this.addCloseEvent(this.args.callback);
		this.addEventInput();
	}

	createInput() {
		this.input = document.createElement('input');
		this.input.classList.add('bubble-alert-notify-input');

		if (this.args.placeholder) {
			this.input.setAttribute('placeholder', this.args.placeholder);
		}

		this.footer.appendChild(this.input);
	}

	addEventInput() {
		if (this.args.keyCodeEnd) {
			this.input.addEventListener('keyup', (e) => {
				if (e.keyCode === this.args.keyCodeEnd) {
					this.args.callback({
						value: this.input.value,
						alert: this
					});
				}
			})
		}
		else {
			this.input.addEventListener('keyup', (e) => {
				this.args.callback({
					value: this.input.value,
					alert: this
				});
			})
		}
	}
}

class NotificationBubbleDialog extends NotificationBubble {
	constructor(title, message, type, args) {
		super(title, message, type);
		this.args = args;
		this.btnTrue = null;
		this.btnFalse = null;
		this.createFooter();
		this.createBtn();
		this.createCloseBtn();
		this.addCloseEvent(this.args.callback);
		this.addEventBtn();
	}

	createBtn() {
		this.btnTrue = document.createElement('button');
		this.btnFalse = document.createElement('button');

		this.btnTrue.classList.add('bubble-alert-notify-btn', '-true');
		this.btnFalse.classList.add('bubble-alert-notify-btn', '-false');

		this.btnTrue.appendChild(document.createTextNode(this.args.btnTrue.text));
		this.btnFalse.appendChild(document.createTextNode(this.args.btnFalse.text));

		this.footer.appendChild(this.btnTrue);
		this.footer.appendChild(this.btnFalse);
	}

	addEventBtn() {
		this.btnTrue.addEventListener('click', () => {
			this.args.callback({
				status: true,
			});

			this.active = false
		});

		this.btnFalse.addEventListener('click', () => {
			this.args.callback({
				status: false,
			});

			this.active = false
		})
	}
}

let BubbleType = {
	info: '--info',
	error: '--error',
	success: '--success',
	danger: '--danger',
	warning: '--warning',
	primary: '--primary',
	default: '--default'
};

export {
	NotificationBubbleModule,
	NotificationBubbleAlert,
	NotificationBubbleInput,
	NotificationBubbleDialog,
	BubbleType
};