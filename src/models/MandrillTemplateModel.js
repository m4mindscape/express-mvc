export default class MandrillTemplateModel {

	/**
	 * @param {{
	 * 		slug: ?string,
	 * 		name: ?string,
	 * 		code: ?string,
	 * 		publish_code: ?string,
	 * 		published_at: ?string,
	 * 		created_at: ?string,
	 * 		updated_at: ?string
	 * 		draft_updated_at: ?string,
	 * 		publish_name: ?string,
	 * 		labels: string[],
	 * 		text: ?string,
	 * 		publish_text: ?string,
	 * 		subject: ?string,
	 * 		publish_subject: ?string,
	 * 		from_email: ?string,
	 * 		publish_from_email: ?string,
	 * 		from_name: ?string,
	 * 		publish_from_name: ?string
	 * }} template
	 * */

	constructor(template) {
		this._slug = template.slug;
		this._name = template.name;
		this._code = template.code;
		this._publishCode = template.publish_code;
		this._publisedAt = template.published_at;
		this._createdAt = template.created_at;
		this._updatedAt = template.updated_at;
		this._draftUpdatedAt = template.draft_updated_at;
		this._publishName = template.publish_name;
		this._labels = template.labels;
		this._text = template.text;
		this._publishText = template.publish_text;
		this._subject = template.subject;
		this._publishSubject = template.publish_subject;
		this._fromEmail = template.from_email;
		this._publishFromEmail = template.publish_from_email;
		this._fromName = template.from_name;
		this._publishFromName = template.publish_from_name;
		this._custom = null;
		this._remove = false;
	}

	get slug() {
		return this._slug
	}

	set slug(value) {
		this._slug = value
	}

	get name() {
		return this._name
	}

	set name(value) {
		this._name = value
	}

	get code() {
		return this._code
	}

	set code(value) {
		this._code = value
	}

	get publishCode() {
		return this._publishCode
	}

	set publishCode(value) {
		this._publishCode = value
	}

	get publisedAt() {
		return this._publisedAt
	}

	set publisedAt(value) {
		this._publisedAt = value
	}

	get createdAt() {
		return this._createdAt
	}

	set createdAt(value) {
		this._createdAt = value
	}

	get updatedAt() {
		return this._updatedAt
	}

	set updatedAt(value) {
		this._updatedAt = value
	}

	get draftUpdatedAt() {
		return this._draftUpdatedAt
	}

	set draftUpdatedAt(value) {
		this._draftUpdatedAt = value
	}

	get publishName() {
		return this._publishName
	}

	set publishName(value) {
		this._publishName = value
	}

	get labels() {
		return this._labels
	}

	set labels(value) {
		this._labels = value
	}

	get text() {
		return this._text
	}

	set text(value) {
		this._text = value
	}

	get publishText() {
		return this._publishText
	}

	set publishText(value) {
		this._publishText = value
	}

	get subject() {
		return this._subject
	}

	set subject(value) {
		this._subject = value
	}

	get publishSubject() {
		return this._publishSubject
	}

	set publishSubject(value) {
		this._publishSubject = value
	}

	get fromEmail() {
		return this._fromEmail
	}

	set fromEmail(value) {
		this._fromEmail = value
	}

	get publishFromEmail() {
		return this._publishFromEmail
	}

	set publishFromEmail(value) {
		this._publishFromEmail = value
	}

	get fromName() {
		return this._fromName
	}

	set fromName(value) {
		this._fromName = value
	}

	get publishFromName() {
		return this._publishFromName
	}

	set publishFromName(value) {
		this._publishFromName = value
	}

	get custom() {
		return this._custom
	}

	set custom(value) {
		this._custom = value
	}

	get remove() {
		return this._remove
	}

	set remove(value) {
		this._remove = value
	}
}