let IControllerInit = Symbol('IControllerInit');
let IControllerRouter = Symbol('IControllerRouter');
let IControllerRoutes = Symbol('IControllerRoutes');
let IControllerExpress = Symbol('IControllerExpress');
let IControllerBaseUrl = Symbol('IControllerBaseUrl');
let IControllerRouterHelper = Symbol('IControllerRouterHelper');
let IControllerRegistration = Symbol('IControllerRegistration');
let IControllerAddRout = Symbol('IControllerAddRout');

export {
    IControllerInit,
    IControllerRegistration,
    IControllerRouter,
    IControllerExpress,
    IControllerBaseUrl,
    IControllerRoutes,
    IControllerRouterHelper,
    IControllerAddRout
};