import {
	IControllerAddRout,
	IControllerExpress,
	IControllerRouter,
	IControllerRouterHelper,
} from "./InterfaceController";
import {IContainer, IContainerSet} from "../container/InterfaceDi";
import ServiceRouterHelper from "../../services/ServiceRouterHelper";

class Controller {
	[IContainerSet](container) {
		this[IContainer] = container;
		this[IControllerRouter] = this[IContainer].get('router');
		this[IControllerExpress] = this[IContainer].get('express');
		this[IControllerRouterHelper] = this[IContainer].get(ServiceRouterHelper);
	}

	[IControllerAddRout](router) {
		router.controller = this;
		this[IControllerRouterHelper].set(router);
	}
}

export default Controller;