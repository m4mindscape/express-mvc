let IContainer = Symbol('Container');
let IContainerSet = Symbol('SetContainer');
let IContainerInitInjection = Symbol('InitInjection');
let IContainerInjectionArgs = Symbol('Args');
let IContainerSetArgs = Symbol('SetArgs');

export {IContainer, IContainerSet, IContainerInitInjection, IContainerSetArgs, IContainerInjectionArgs};