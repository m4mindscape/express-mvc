import {IContainerInjectionArgs, IContainer, IContainerInitInjection, IContainerSetArgs, IContainerSet} from "./InterfaceDi";

const container = Symbol('container');

class DI {
	constructor() {
		this[container] = new Map();
	}

	set(key, value) {
		if (!this.has(key)) {
			this[container].set(key, value);
		}
		else {
			console.warn(`DI Container: key find in container  ${key}`);
		}
	}

	setObject(value, args) {
		let key = value;
		value = new value();
		if (!this.has(key)) {
			if (value[IContainerSet] !== undefined || value[IContainer] !== undefined) {
				if (value[IContainerSet] !== undefined) {
					value[IContainerSet](this);
				}
				else if (value[IContainer] !== undefined) {
					value[IContainer] = this;
				}
			}

			if (args) {
				if (value[IContainerSetArgs] !== undefined) {
					value[IContainerSetArgs](args);
				}
				else if (value[IContainerInjectionArgs] !== undefined) {
					value[IContainerInjectionArgs] = args;
				}
			}

			if (value[IContainerInitInjection] !== undefined) {
				value[IContainerInitInjection]();
			}
			this[container].set(key, value);
		}
		else {
			console.warn(`DI Container: key find in container  ${key}`);
		}
	}

	get(key) {
		return this[container].get(key);
	}

	has(key) {
		return this[container].has(key);
	}
}

export default DI;