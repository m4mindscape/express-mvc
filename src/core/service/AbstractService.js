import {
    IContainerInjectionArgs,
    IContainer,
    IContainerInitInjection,
    IContainerSetArgs,
    IContainerSet
} from "../container/InterfaceDi";

class AbstractService {
    [IContainerSet](DI) {
        this[IContainer] = DI;
    }

    [IContainerInitInjection]() {
        console.dir(`Init: ${this.constructor.name}`);
    }

    [IContainerSetArgs](args) {
        this[IContainerInjectionArgs] = args;
    }
}

export default AbstractService;