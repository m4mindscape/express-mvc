class Router {
    constructor(url, method, callback) {
        this._url = url;
        this._method = method;
        this._callback = callback;
        this._controller = null;
        this._req = null;
        this._res = null;
        this._next = null;
        this._params = null;
    }

    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }

    get method() {
        return this._method;
    }

    set method(value) {
        this._method = value;
    }

    get callback() {
        return this._callback;
    }

    set callback(value) {
        this._callback = value;
    }

    get controller() {
        return this._controller
    }

    set controller(value) {
        this._controller = value
    }

    get req() {
        return this._req;
    }

    set req(value) {
        this._req = value;
    }

    get res() {
        return this._res;
    }

    set res(value) {
        this._res = value;
    }

    get next() {
        return this._next;
    }

    set next(value) {
        this._next = value;
    }

    get params() {
        return this._params;
    }

    set params(value) {
        this._params = value;
    }

    getParam(param) {
        return this.params[param];
    }
}

class RouterGet extends Router {
    constructor(url, callback) {
        super(url, RouterMethods.get, callback);
    }
}

class RouterPost extends Router {
    constructor(url, callback) {
        super(url, RouterMethods.post, callback);
    }
}

let RouterMethods = {
    get: 'get',
    post: 'post',
}

export {Router, RouterGet, RouterPost, RouterMethods};