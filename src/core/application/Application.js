import {IContainer, IContainerInitInjection, IContainerSet} from "../container/InterfaceDi";
import express                                              from 'express';
import http                                                 from 'http';

let run = Symbol('run');
let server = Symbol('server');
let config = Symbol('config');

class Application {
    constructor() {
        this[IContainer] = null;
        this.express = express();
        this.router = express.Router();
        this[server] = null;
    }

    [IContainerInitInjection]() {
        this[IContainer].set('express', this.express);
        this[IContainer].set('router', this.router);

        this[config] = this[IContainer].get('CONFIG');

        this.express.set('views', this[config].views.path);
        this.express.set('view engine', this[config].views.engine);
        this.express.set('view cache', this[config].views.cache);
        this.express.use(express.static(this[config].public));

        this[run]();

    }

    [run]() {

        this.express.set('port', this[IContainer].get('PORT'));

        this[server] = http.createServer(this.express);
        this[server].listen(this[IContainer].get('PORT'));

        this[server].on('error', err => {
            console.log(err);
        })
    }
}

export default Application;