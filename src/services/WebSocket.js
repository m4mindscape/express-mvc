import {server} from 'websocket';
import {IContainer} from "../core/container/InterfaceDi"

export default class WebSocket {
	constructor() {
		this[IContainer] = null;
		this.server = new server()
	}
}