import AbstractService from "../core/service/AbstractService"
import {IContainerInitInjection} from "../core/container/InterfaceDi"
import fs from 'fs';
import path from 'path'

export default class RtbRegexToSite extends AbstractService {
	[IContainerInitInjection]() {
		this.PathYml = 'C:\\projects\\miro\\site\\data\\localization';
		this.PathYmlLang = 'C:\\projects\\miro\\site\\data\\language';
		this.twig = 'C:\\projects\\miro\\site\\application\\views';

		this.regExp = new RegExp('(?!(http(s?):\\/\\/|-))((?!(\\.+))(R|r)ealtime(B|b)oard(?!(\\w+|\\.+)))(?!((\\.)(com|ru)|-))', 'gm');
		this.fileCollection = [];


		this.recursionFindFileInFolder(this.PathYml, this.fileCollection, (file) => {
			fs.readFile(file, 'utf8', (err, data) => {
				data = data.replace(this.regExp, "Miro");

				fs.writeFile(file, data, (err) => {
					if (err) throw err;
				})
			})
		});
		this.recursionFindFileInFolder(this.PathYmlLang, this.fileCollection, (file) => {
			fs.readFile(file, 'utf8', (err, data) => {
				data = data.replace(this.regExp, "Miro");

				fs.writeFile(file, data, (err) => {
					if (err) throw err;
				})
			})
		});

		this.recursionFindFileInFolder(this.twig, this.fileCollection, (file) => {
			fs.readFile(file, 'utf8', (err, data) => {
				data = data.replace(this.regExp, "Miro");

				fs.writeFile(file, data, (err) => {
					if (err) throw err;
				})
			})
		});

		// this.recursionFindFileInFolder(this.)
	}

	recursionFindFileInFolder(pathDir, collection, callback) {
		fs.readdir(pathDir, (err, list) => {
			if (err) {
				throw err;
			}

			if (!list.length) {
				return;
			}

			list.forEach(file => {
				file = path.resolve(pathDir, file);
				fs.stat(file, (err, stat) => {
					if (stat && stat.isDirectory()) {
						this.recursionFindFileInFolder(file, collection, callback);
					}
					else {
						collection.push(file);
						callback(file);
					}
				})
			});
		})
	}
}