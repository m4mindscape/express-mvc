import {IContainer, IContainerInitInjection} from "../core/container/InterfaceDi"
import {Mandrill} from 'mandrill-api';
import MandrillTemplatesRepository from "../repository/MandrillTemplatesRepository"
import MandrillTemplateModel from "../models/MandrillTemplateModel";
import Redesign from "../helpers/Redesign"
import RedesignNew from "../helpers/RedesignNew"
import fsLib from 'fs';
import ReplaceMeta from "../helpers/ReplaceMeta"

let templates = require('../../templates4');

export default class MandrillService {
	constructor() {
		this[IContainer] = null;
	}

	[IContainerInitInjection]() {
		this.key = 'w41fFTp0NDKr2mtsNnaL-w';
		this.mandrill = new Mandrill(this.key);
		this.mandrill.debug = true;
		this[IContainer].setObject(MandrillTemplatesRepository);
		this.repository = this[IContainer].get(MandrillTemplatesRepository);
		// this.loadAllTemplates()
		this.loadAllTemplatesFromFile();
	}

	loadAllTemplates() {
		this.mandrill.templates.list({}, result => {
			console.timeEnd();
			console.time();

			console.dir(this.mandrill);

			for (let template of result) {
				this.repository.add(new MandrillTemplateModel(template));
			}

			console.log('Success load templates');
			console.timeEnd();
		}, (e) => {

			console.dir(e);
			console.timeEnd();
		})
	}

	loadAllTemplatesFromFile() {
		for (let template of templates) {
			let templateObj = new MandrillTemplateModel(template);
			this.repository.add(templateObj);
			ReplaceMeta.replaceMeta(templateObj);

			if (templateObj.slug.indexOf('miro-') <= -1 && templateObj.slug.indexOf('site-') <= -1) {
				templateObj.remove = true;
			}

		}
	}

	/**
	 * @param {MandrillTemplateModel[]} templates;
	 * */
	uploadTemplates(templates) {

		let count = 0;
		let list = templates;
		let _this = this;

		if (list.length > 0) {
			setInterval(function () {
				_this.upload(list[count])
				count++;
			}, 1000)
		}
	}

	/**
	 * @param {MandrillTemplateModel} template;
	 * */
	updateTemplate(template, callback) {
		this.mandrill.templates.update({
			slug: template.slug,
			code: template.code,
			labels: template.labels,
			name: template.name,
			from_name: template.fromName,
			subject: template.subject
		}, (result) => {
			callback(result);
		}, (error) => {
			callback(error);
		})
	}

	/**
	 * @param {MandrillTemplateModel} template;
	 * */
	upload(template) {

		this.mandrill.templates.add({
			slug: template.slug,
			name: template.name,
			code: template.code,
			subject: template.subject,
			from_email: template.fromEmail,
			from_name: template.fromName
		}, (result) => {
			console.log(result);
		}, (error) => {
			console.log(error);
		})

	}

	/**
	 * @param {string} slug
	 * */
	remove(slug, callback) {
		this.mandrill.templates.delete({"name": slug}, (result) => {
			callback(result);
		}, (error) => {
			callback(error);
		})
	}
}