import {IContainer, IContainerInitInjection} from "../core/container/InterfaceDi";
import AbstractService from "../core/service/AbstractService";
import {RouterMethods} from "../core/application/Router"

let express = Symbol('express');
let router = Symbol('router');
let urls = Symbol('urls');
let AddRout = Symbol('AddRout');

export default class ServiceRouterHelper extends AbstractService {
	[IContainerInitInjection]() {
		this[express] = this[IContainer].get('express');
		this[router] = this[IContainer].get('router');
		this[urls] = new Map();
	}

	set(Router) {
		if (!this[urls].has(Router.url)) {
			this[urls].set(Router.url, []);
		}

		this[urls].get(Router.url).push(Router);
	}

	registration() {
		let map = [... this[urls].keys()].sort((a, b) => {
			a = a === '*' ? 0 : a.length;
			b = b === '*' ? 0 : b.length;

			return b - a;
		});

		for (let url of map) {
			for (let router of this[urls].get(url)) {
				this[AddRout](router)
			}
		}
	}

	[AddRout](Router) {
		this[express].use(this[router][Router.method](Router.url, async (req, res, next) => {
			Router.req = req;
			Router.res = res;
			Router.next = next;
			Router.params = req.params;
			Router.callback();
		}))
	}
}