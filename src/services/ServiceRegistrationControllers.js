import AbstractService from "../core/service/AbstractService";
import {IContainer, IContainerInitInjection, IContainerSet} from "../core/container/InterfaceDi";
import fs from "fs";
import {IControllerInit} from "../core/controller/InterfaceController";
import ErrorController from "../controllers/ErrorController"
import ServiceRouterHelper from "./ServiceRouterHelper"

const injection = Symbol('Injection');

export default class ServiceRegistrationControllers extends AbstractService {
    [IContainerInitInjection]() {
        let pathControllers = `${this[IContainer].get("DIR")}\\controllers`;
        fs.readdir(pathControllers, (err, item) => {
            for (let file of item) {
                let controller = require(`${pathControllers}\\${file}`);
                controller = new controller.default();
                this[injection](controller);
            }
            this[IContainer].get(ServiceRouterHelper).registration();
        })
    }

    [injection](controller) {
        if (controller[IContainerSet] !== undefined) {
            controller[IContainerSet](this[IContainer]);
        }

        if (controller[IControllerInit] !== undefined) {
            controller[IControllerInit]();
        }
    }
}