import {IContainerInitInjection, IContainerInjectionArgs, IContainerSetArgs} from "../../../core/container/InterfaceDi";
import {Client} from "./client/Client"
import net from 'net'

export default class Server {
	constructor() {
		this.server = null;
		this.clients = new Map();
	}

	[IContainerSetArgs](args) {
		this[IContainerInjectionArgs] = args;
		this.port = args.port;
	}

	[IContainerInitInjection]() {
		this.server = net.createServer(socket => {
			let client = new Client(socket);

			this.clients.has(!client.getName())
			{
				this.clients.set(client.getName(), client);
			}
		})

		this.server.listen(this.port, () => {
			console.log("TCP Start")
		})
	}
}