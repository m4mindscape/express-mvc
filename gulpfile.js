let gulp = require('gulp');
let {series, parallel, watch} = require('gulp');
let less = require('gulp-less');
let browserSync = require('browser-sync').create();
let spawn = require('child_process').spawn;
let concat = require('gulp-concat');
let uglify = require('gulp-uglify');
let uglifyCSS = require('gulp-uglifycss');

const webpackStream = require('webpack-stream');

let paths = {
	less: './src/public/css/src/**',
	lessCompilePath: './src/public/css/dist/',
	twig: './src/views/**',
	js: './src/public/js/src/**',
	jsCompilePath: './src/public/js/dist/',
	backend: ['./src/controllers/**', './src/core/**', './src/services/**', './src/app.js', './src/modules/**']
};

gulp.task('run-server', (cb) => {
	let run = spawn('powershell.exe', ['npm run dev'], {stdio: 'inherit'});
	run.on('data', (data) => {
		console.log(data);
	})
	cb();
});

gulp.task('browser-sync', gulp.parallel(() => {
		browserSync.init({
			proxy: 'localhost:3500',
			port: 8090,
			open: true,
			notify: false
		});
	}
));

gulp.task('less-compile', () => {
	return gulp.src(paths.less)
		.pipe(less())
		.pipe(concat('style.css'))
		.pipe(gulp.dest(paths.lessCompilePath))
		.pipe(uglifyCSS())
		.pipe(browserSync.stream());
});

gulp.task('twig-watch', () => {
	return gulp.src(paths.twig)
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('js-watch', () => {
	return gulp.src(paths.js)
		.pipe(webpackStream({
			mode: 'development',
			output: {
				filename: 'common.js'
			}
		}))
		.pipe(gulp.dest(paths.jsCompilePath))
		.pipe(uglify())
		.pipe(browserSync.reload({stream: true}))
});

gulp.task('browser-reload', () => {
	return gulp.src(paths.backend)
		.pipe(browserSync.reload({stream: true}))
})


gulp.task('watch', gulp.parallel(() => {
	watch(paths.less, gulp.parallel('less-compile'));
	watch(paths.twig, gulp.parallel('twig-watch'));
	watch(paths.js, gulp.parallel('js-watch'));
	watch(paths.backend, gulp.parallel('browser-reload'))
}));

gulp.task('dev', gulp.series('run-server', gulp.parallel('browser-sync', 'watch')));